import sys

inputFile = "./inputArray.txt"
if (len(sys.argv) > 1):
    inputFile = sys.argv[1];

def loadTriangle():
    triangle = [];
    with open(inputFile) as f: 
        for line in f: 
            triangle.append([int(num) for num in line.split(',')]);
        
    return triangle;

triangle = loadTriangle();

maxPath = [];
lastIdx = 0;

for idx,item in enumerate(triangle):
    maxPath.append(max(item[lastIdx:lastIdx+2]));
    lastIdx = item.index(maxPath[-1]);
    
print(triangle);
print(maxPath);
print(sum(maxPath));